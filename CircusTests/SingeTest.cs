using Microsoft.VisualStudio.TestTools.UnitTesting;
using CircusConsoleApp1;

namespace CircusTests
{
    [TestClass]
    public class SingeTests
    {
        Singe singe = new Singe("Moukie");

        [TestMethod]
        public void TestSingeObject()
        {
            Assert.IsNotNull(singe.nom);
            Assert.AreEqual(singe.nom, "Moukie");
        }

        [TestMethod]
        public void TestSingeMethod()
        {
            Assert.IsNull(singe.getTourEnCour());
            singe.FaitUnTour(1);
            Tour tour = singe.getTourEnCour();
            Assert.IsNotNull(tour);
            Assert.AreEqual(tour.nom, "Chante");
        }

        [TestMethod]
        [ExpectedException(typeof(ShowException))]
        public void TestSingeThowMethod()
        {
                singe.FaitUnTour(15);
        }
    }
}
