using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CircusConsoleApp1;

namespace CircusTests
{
    [TestClass]
    public class SpectateurTests
    {
        private Dresseur dresseur;
        private Spectateur spectateur;
        public SpectateurTests()
        {
            Singe singe = new Singe("Moukie");
            this.dresseur = new Dresseur(singe);
            this.spectateur = new Spectateur();
        }

        [TestMethod]
        public void TestSpectateurMethod()
        {
            Assert.IsTrue(dresseur.nextTour());
            Tour tour = dresseur.singe.getTourEnCour();
            Spectateur.Reaction reaction = spectateur.react(tour);
            switch (tour.getType())
            {
                case Tour.Type.acrobatie:
                    {
                        Assert.AreEqual(reaction, Spectateur.Reaction.applaudit);
                        Console.WriteLine("applaudit");
                        break;
                    }
                case Tour.Type.musique:
                    {
                        Assert.AreEqual(reaction, Spectateur.Reaction.siffle);
                        Console.WriteLine("siffle");
                        break;
                    }
                default:
                    { break; }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ShowException))]
        public void TestSpectateurReactThow()
        {
            spectateur.react(null);
        }

    }
}
