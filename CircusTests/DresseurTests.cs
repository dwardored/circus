using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CircusConsoleApp1;

namespace CircusTests
{
    [TestClass]
    public class DresseurTests
    {
        private Dresseur dresseur;
        private Spectateur spectateur;
        public DresseurTests()
        {
            Singe singe = new Singe("Moukie");
            this.dresseur = new Dresseur(singe);
            this.spectateur = new Spectateur();
        }

        [TestMethod]
        public void TestDresseurMethod()
        {
            bool ex = dresseur.nextTour();
            Assert.IsTrue(ex);
            Tour tour1 = dresseur.singe.getTourEnCour();
            ex = dresseur.nextTour();
            Assert.IsTrue(ex);
            Tour tour2 = dresseur.singe.getTourEnCour();
            Assert.AreNotEqual(tour2, tour1);
        }

        [TestMethod]
        public void TestDresseurMethodLoopTour()
        {
            int toursCount = dresseur.singe.tours.Count;
            for(int i = 0; i < toursCount; i++)
            {
                Assert.IsTrue(dresseur.nextTour()); 
            }
            Assert.IsFalse(dresseur.nextTour());
        }

    }
}
