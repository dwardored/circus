﻿using System;
using System.Collections.Generic;

namespace CircusConsoleApp1
{
    public class Spectateur
    {
        public enum Reaction
        {
            applaudit,
            siffle,
            inactif
        }

        public Dictionary<Tour.Type, Reaction> reactions_dico = new Dictionary<Tour.Type, Reaction> { 
            { Tour.Type.acrobatie, Reaction.applaudit },
            { Tour.Type.musique, Reaction.siffle }
        };
        
        private Reaction reaction = Reaction.inactif;
        public Spectateur()
        {

        }

        public Reaction react(Tour tour)
        {
            if (tour != null)
            {
                this.reaction = reactions_dico[tour.getType()];
                return this.reaction;
            }
            else
            {
                throw new ShowException("Tour is null");
            }
        }
        public Reaction getReaction()
        { return this.reaction; }

    }
}
