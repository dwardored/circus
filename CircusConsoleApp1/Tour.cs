﻿using System;
using System.Collections.Generic;

namespace CircusConsoleApp1
{
    public class Tour
    {
        public string nom { get; }
        private Type type;

        public enum Type
        {
            acrobatie,
            musique
        }

        private Dictionary<Type, String> typePhrase = new Dictionary<Type, string>() { { Type.acrobatie, "le tour d'acrobatie" }, { Type.musique, "le tour de musique" } };

        public Tour(string nom, Type type)
        {
            this.nom = nom;
            this.type = type;
        }
        public Type getType() {
            return this.type;
        }

        public String getTypeDePhrase()
        {
            return typePhrase[type];
        }

        public override bool Equals(object obj)
        {
            return obj is Tour tour &&
                   nom == tour.nom &&
                   type == tour.type &&
                   EqualityComparer<Dictionary<Type, string>>.Default.Equals(typePhrase, tour.typePhrase);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(nom, type, typePhrase);
        }
    }
}
