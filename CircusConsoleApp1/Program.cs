﻿using System;
using System.Collections.Generic;

namespace CircusConsoleApp1
{
    class Circus
    {
      
        static void Main(string[] args)
        {
            Spectateur spectateur = new Spectateur();
            Show show = new Show(spectateur);
            show.start();
        }
    }
}
