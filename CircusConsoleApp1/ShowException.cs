﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusConsoleApp1
{
    using System;

    public class ShowException : Exception
    {
        public ShowException()
        {
        }

        public ShowException(string message)
            : base(message)
        {
        }

        public ShowException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
