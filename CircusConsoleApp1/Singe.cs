﻿using System;
using System.Collections.Generic;

namespace CircusConsoleApp1
{
    public class Singe
    {
        public string nom { get; }

        private Tour tour_en_cour;

        public Tour getTourEnCour()
        {
            return this.tour_en_cour;
        }
        public List<Tour> tours { get; } = new List<Tour>() {
            new Tour("Marche sur les Mains", Tour.Type.acrobatie),
            new Tour("Chante", Tour.Type.musique),
            new Tour("Danse", Tour.Type.acrobatie),
            new Tour("Joue des maracasse", Tour.Type.musique),
            new Tour("Jongle avec 3 balles", Tour.Type.acrobatie)
        };

        public Singe(string name)
        {
            this.nom = name;
            this.tours = tours;
        }
        /**
         * Si on veut un singe avec des tours différents
         */
        public Singe(string name, List<Tour> tours)
        { //si on veut un singe avec des tours d
            this.nom = name;
            this.tours = tours;
        }

        public void FaitUnTour(int index_du_tour)
        {
            if(index_du_tour > this.tours.Count-1)
            {
                Console.WriteLine("OuououoHahaha, le singe ${0} ne semble pas connaitre ce tour", nom);
                throw new ShowException("Le singe ne connait pas le tour demandé");
            }
            else
            {
                this.tour_en_cour = this.tours[index_du_tour];
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Singe singe &&
                   nom == singe.nom &&
                   EqualityComparer<Tour>.Default.Equals(tour_en_cour, singe.tour_en_cour) &&
                   EqualityComparer<List<Tour>>.Default.Equals(tours, singe.tours);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(nom, tour_en_cour, tours);
        }
    }
}
