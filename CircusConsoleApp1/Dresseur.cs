﻿using System;
using System.Collections.Generic;

namespace CircusConsoleApp1
{
    public class Dresseur
    {
        public Singe singe {get;} 
        private int current_tour_index = 0;

        public Dresseur(Singe singe)
        {
            this.singe = singe;
        }

        public bool nextTour()
        {
            if (singe.tours.Count > current_tour_index)
            {
                singe.FaitUnTour(current_tour_index);
                current_tour_index++;
                return true;
            }
            return false;
        }
        public void resetShow()
        {
            current_tour_index = 0;
        }

        public override bool Equals(object obj)
        {
            return obj is Dresseur dresseur &&
                   EqualityComparer<Singe>.Default.Equals(singe, dresseur.singe) &&
                   current_tour_index == dresseur.current_tour_index;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(singe, current_tour_index);
        }
    }
}