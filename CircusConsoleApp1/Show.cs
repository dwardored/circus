﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusConsoleApp1
{
    class Show
    {
        private Spectateur spectateur;
        private List<Dresseur> dresseurs;
        public Show(Spectateur spectateur)
        {
            if (spectateur != null)
            {
                this.spectateur = spectateur;
                Singe singe1 = new Singe("Singe1");

                //faire faire des tours diférent au singe2
                List<Tour> tours2 = new List<Tour>() {
                new Tour("Siffle une mélodie", Tour.Type.musique),
                new Tour("Fait des saltos arriéres", Tour.Type.acrobatie),
                new Tour("Fait du tamtam", Tour.Type.musique)
            };
                Singe singe2 = new Singe("Singe2", tours2);

                Dresseur dresseur1 = new Dresseur(singe1);
                Dresseur dresseur2 = new Dresseur(singe2);

               this.dresseurs = new List<Dresseur>() { new Dresseur(singe1), new Dresseur(singe2) };
            }
            else {
                throw new ShowException("No spectateur");
            }
        }

        public void start()
        {
            bool continueShow = true;
            while (continueShow)
            {
                continueShow = false;
                foreach (Dresseur dresseur in this.dresseurs)
                {
                    if (dresseur.nextTour())
                    {
                        continueShow = true;
                        Tour tour = dresseur.singe.getTourEnCour();
                        this.spectateur.react(tour);
                        Console.WriteLine("Spectateur {0} pendant {1} '{2}' du {3}", this.spectateur.getReaction(), tour.getTypeDePhrase(), tour.nom, dresseur.singe.nom);
                    }
                }

            }
        }

    }

}
